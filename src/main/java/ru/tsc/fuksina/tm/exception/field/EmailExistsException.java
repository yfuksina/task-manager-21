package ru.tsc.fuksina.tm.exception.field;

public final class EmailExistsException extends AbstractFieldException {

    public EmailExistsException() {
        super("Error! Email exists...");
    }

}
