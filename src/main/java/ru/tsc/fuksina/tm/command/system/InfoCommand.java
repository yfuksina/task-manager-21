package ru.tsc.fuksina.tm.command.system;

import ru.tsc.fuksina.tm.util.ByteUtil;

public final class InfoCommand extends AbstractSystemCommand {

    public static final String NAME = "info";

    public static final String DESCRIPTION = "Show system info";

    public static final String ARGUMENT = "-i";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        final Runtime runtime = Runtime.getRuntime();
        final int availableProcessors = runtime.availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = runtime.freeMemory();
        final String freeMemoryFormat = ByteUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);
        final long maxMemory = runtime.maxMemory();
        final String maxMemoryValue = ByteUtil.formatBytes(maxMemory);
        final boolean isMemoryLimit = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = isMemoryLimit ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = runtime.totalMemory();
        final String totalMemoryFormat = ByteUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVN: " + totalMemoryFormat);
        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = ByteUtil.formatBytes(usedMemory);
        System.out.println("Used memory in JVN: " + usedMemoryFormat);
    }

}
