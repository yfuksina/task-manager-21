package ru.tsc.fuksina.tm.command;

import ru.tsc.fuksina.tm.api.model.ICommand;
import ru.tsc.fuksina.tm.api.service.IAuthService;
import ru.tsc.fuksina.tm.api.service.IServiceLocator;
import ru.tsc.fuksina.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    public abstract String getName();

    public abstract String getDescription();

    public abstract String getArgument();

    public abstract Role[] getRoles();

    public abstract void execute();

    protected IServiceLocator serviceLocator;

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public String getUserId() {
        return getAuthService().getUserId();
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

}
