package ru.tsc.fuksina.tm.api.service;

import ru.tsc.fuksina.tm.api.repository.IRepository;
import ru.tsc.fuksina.tm.enumerated.Sort;
import ru.tsc.fuksina.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    List<M> findAll(Sort sort);
}
